local func = {}

-- 
function func.get_mode()
  return vehicle:get_mode()
end

--
function func.send_text(msg)
  gcs:send_text(0, msg)
end

return func
