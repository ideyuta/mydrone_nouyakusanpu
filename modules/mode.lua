local copter_mode = {}
copter_mode["0"] = "COPTER_MODE_STABILIZE"
copter_mode["1"] = "COPTER_MODE_ACRO"
copter_mode["2"] = "COPTER_MODE_ALT_HOLD"
copter_mode["3"] = "COPTER_MODE_AUTO"
copter_mode["4"] = "	COPTER_MODE_GUIDED"
copter_mode["5"] = "COPTER_MODE_LOITER"
copter_mode["6"] = "COPTER_MODE_RTL"
copter_mode["7"] = "COPTER_MODE_CIRCLE"
copter_mode["8"] = "INVALID_MODE"
copter_mode["9"] = "COPTER_MODE_LAND"
copter_mode["10"] = "INVALID_MODE"
copter_mode["11"] = "COPTER_MODE_DRIFT"
copter_mode["12"] = "INVALID_MODE"
copter_mode["13"] = "COPTER_MODE_SPORT"
copter_mode["14"] = "COPTER_MODE_FLIP"
copter_mode["15"] = "COPTER_MODE_AUTOTUNE"
copter_mode["16"] = "COPTER_MODE_POSHOLD"
copter_mode["17"] = "COPTER_MODE_BRAKE"
copter_mode["18"] = "COPTER_MODE_THROW"
copter_mode["19"] = "COPTER_MODE_AVOID_ADSB"
copter_mode["20"] = "COPTER_MODE_GUIDED_NOGPS"
copter_mode["21"] = "COPTER_MODE_SMART_RTL"
copter_mode["22"] = "COPTER_MODE_FLOWHOLD"
copter_mode["23"] = "COPTER_MODE_FOLLOW"
copter_mode["24"] = "COPTER_MODE_ZIGZAG"
copter_mode["25"] = "COPTER_MODE_SYSTEMID"
copter_mode["26"] = "COPTER_MODE_AUTOROTATE"
copter_mode["27"] = "COPTER_MODE_AUTO_RTL"
