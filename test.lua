-- Logger
local log_name = "nouyakusanpu.csv"
local log = io.open(log_name, "a")
if not log then
  gcs:send_text(3, string.format("Can not open %s", log_name))
end
local Logger = {
  Write = function(content)
    log:write(tostring(content))
    log:flush()
  end
}

-- copter_mode
local copter_mode = {}
copter_mode[0] = "COPTER_MODE_STABILIZE"
copter_mode[1] = "COPTER_MODE_ACRO"
copter_mode[2] = "COPTER_MODE_ALT_HOLD"
copter_mode[3] = "COPTER_MODE_AUTO"
copter_mode[4] = "COPTER_MODE_GUIDED"
copter_mode[5] = "COPTER_MODE_LOITER"
copter_mode[6] = "COPTER_MODE_RTL"
copter_mode[7] = "COPTER_MODE_CIRCLE"
copter_mode[8] = "INVALID"
copter_mode[9] = "COPTER_MODE_LAND"
copter_mode[10] = "INVALID"
copter_mode[11] = "COPTER_MODE_DRIFT"
copter_mode[12] = "INVALID"
copter_mode[13] = "COPTER_MODE_SPORT"
copter_mode[14] = "COPTER_MODE_FLIP"
copter_mode[15] = "COPTER_MODE_AUTOTUNE"
copter_mode[16] = "COPTER_MODE_POSHOLD"
copter_mode[17] = "COPTER_MODE_BRAKE"
copter_mode[18] = "COPTER_MODE_THROW"
copter_mode[19] = "COPTER_MODE_AVOID_ADSB"
copter_mode[20] = "COPTER_MODE_GUIDED_NOGPS"
copter_mode[21] = "COPTER_MODE_SMART_RTL"
copter_mode[22] = "COPTER_MODE_FLOWHOLD"
copter_mode[23] = "COPTER_MODE_FOLLOW"
copter_mode[24] = "COPTER_MODE_ZIGZAG"
copter_mode[25] = "COPTER_MODE_SYSTEMID"
copter_mode[26] = "COPTER_MODE_AUTOROTATE"
copter_mode[27] = "COPTER_MODE_AUTO_RTL"

-- function options
local function_options = {}
function_options[-1] = "GPIO"
function_options[0] = "Disabled"
function_options[1] = "RCPassThru"
function_options[2] = "INVALID"
function_options[3] = "INVALID"
function_options[4] = "INVALID"
function_options[5] = "INVALID"
function_options[6] = "MountPan"
function_options[7] = "MountTilt"
function_options[8] = "MountRoll"
function_options[9] = "MountOpen"
function_options[10] = "CameraTrigger"
function_options[11] = "INVALID"
function_options[12] = "Mount2Pan"
function_options[13] = "Mount2Tilt"
function_options[14] = "Mount2Roll"
function_options[15] = "Mount2Open"
function_options[16] = "INVALID"
function_options[17] = "INVALID"
function_options[18] = "INVALID"
function_options[19] = "INVALID"
function_options[20] = "INVALID"
function_options[21] = "INVALID"
function_options[22] = "SprayerPump"
function_options[23] = "SprayerSpinner"
function_options[24] = "INVALID"
function_options[25] = "INVALID"
function_options[26] = "INVALID"
function_options[27] = "Parachute"
function_options[28] = "Gripper"
function_options[29] = "LandingGear"
function_options[30] = "EngineRunEnable"
function_options[31] = "HeliRSC"
function_options[32] = "HeliTailRSC"
function_options[33] = "Motor1"
function_options[34] = "Motor2"
function_options[35] = "Motor3"
function_options[36] = "Motor4"
function_options[37] = "Motor5"
function_options[38] = "Motor6"
function_options[39] = "Motor7"
function_options[40] = "Motor8"
function_options[41] = "INVALID"
function_options[42] = "INVALID"
function_options[43] = "INVALID"
function_options[44] = "INVALID"
function_options[45] = "INVALID"
function_options[46] = "INVALID"
function_options[47] = "INVALID"
function_options[48] = "INVALID"
function_options[49] = "INVALID"
function_options[50] = "INVALID"
function_options[51] = "RCIN1"
function_options[52] = "RCIN2"
function_options[53] = "RCIN3"
function_options[54] = "RCIN4"
function_options[55] = "RCIN5"
function_options[56] = "RCIN6"
function_options[57] = "RCIN7"
function_options[58] = "RCIN8"
function_options[59] = "RCIN9"
function_options[60] = "RCIN10"
function_options[61] = "RCIN11"
function_options[62] = "RCIN12"
function_options[63] = "RCIN13"
function_options[64] = "RCIN14"
function_options[65] = "RCIN15"
function_options[66] = "RCIN16"
function_options[67] = "INVALID"
function_options[68] = "INVALID"
function_options[69] = "INVALID"
function_options[70] = "INVALID"
function_options[71] = "INVALID"
function_options[72] = "INVALID"
function_options[73] = "ThrottleLeft"
function_options[74] = "ThrottleRight"
function_options[75] = "TiltMotorFrontLeft"
function_options[76] = "TiltMotorFrontRight"
function_options[77] = "INVALID"
function_options[78] = "INVALID"
function_options[79] = "INVALID"
function_options[80] = "INVALID"
function_options[81] = "BoostThrottle"
function_options[82] = "Motor9"
function_options[83] = "Motor10"
function_options[84] = "Motor11"
function_options[85] = "Motor12"
function_options[86] = "INVALID"
function_options[87] = "INVALID"
function_options[88] = "Winch"
function_options[89] = "INVALID"
function_options[90] = "CameraISO"
function_options[91] = "CameraAperture"
function_options[92] = "CameraFocus"
function_options[93] = "CameraShutterSpeed"
function_options[94] = "Script1"
function_options[95] = "Script2"
function_options[96] = "Script3"
function_options[97] = "Script4"
function_options[98] = "Script5"
function_options[99] = "Script6"
function_options[100] = "Script7"
function_options[101] = "Script8"
function_options[102] = "Script9"
function_options[103] = "Script10"
function_options[104] = "Script11"
function_options[105] = "Script12"
function_options[106] = "Script13"
function_options[107] = "Script14"
function_options[108] = "Script15"
function_options[109] = "Script16"
function_options[110] = "INVALID"
function_options[111] = "INVALID"
function_options[112] = "INVALID"
function_options[113] = "INVALID"
function_options[114] = "INVALID"
function_options[115] = "INVALID"
function_options[116] = "INVALID"
function_options[117] = "INVALID"
function_options[118] = "INVALID"
function_options[119] = "INVALID"
function_options[120] = "NeoPixel1"
function_options[121] = "NeoPixel2"
function_options[122] = "NeoPixel3"
function_options[123] = "NeoPixel4"
function_options[124] = "RateRoll"
function_options[125] = "RatePitch"
function_options[126] = "RateThrust"
function_options[127] = "RateYaw"
function_options[128] = "INVALID"
function_options[129] = "ProfiLED1"
function_options[130] = "ProfiLED2"
function_options[131] = "ProfiLED3"
function_options[132] = "ProfiLEDClock"
function_options[133] = "WinchClutch"
function_options[134] = "SERVOn_MIN"
function_options[135] = "SERVOn_TRIM"
function_options[136] = "SERVOn_MAX"

-- MavlinkMissionItem = {
--   New = function()
--       return {
--         param1 = 0.0,
--         param2 = 0.0,
--         param3 = 0.0,
--         param4 = 0.0,
--         x      = 0,
--         y      = 0,
--         z      = 0.0,
--         seq    = 0,
--         command           = 0,
--         target_system     = 0,
--         target_component  = 0,
--         frame             = 0,
--         current           = 0,
--         autocontinue      = 0
--       }
--   end
-- }

-- @brief Send text to GCS
-- @param severity
--        0: Emergency
--        1: Alert
--        2: Critical
--        3: Error
--        4: Warning
--        5: Notice
--        6: Info
-- @param msg
function send_text(severity, msg)
  gcs:send_text(severity, msg)
end

-- @brief return true if armed
function is_armed()
  return arming:is_armed()
end

-- @brief Get currenct flight mode
function get_mode()
  return vehicle:get_mode()
end

-- @brief Get PWM output value for servo channel which given function has
-- @param func: servo functions number
function get_output_pwm(func)
  return SRV_Channels:get_output_pwm(func)
end 

-- @brief Get servo functions number for given servo functions name
-- @param str: servo function
function servo_function_options_str_to_index(str)
  for key, value in pairs (function_options) do 
    if value == str then
      return key
    end
  end
  return false
end

-- @brief Set PWM for servo channel which given function has
-- @param func
-- @param pwm
function set_servo_output_pwm(func_str, pwm)
  local servo_func_index = servo_function_options_str_to_index(func_str)
  local servo_ch = SRV_Channels:find_channel(servo_func_index)
  SRV_Channels:set_output_pwm_chan_timeout(servo_ch, pwm, 1000)
end

-- @brief Set PWM for given servo channel
-- @param ch
-- @param pwm
function set_servo_output_pwm_ch(ch, pwm)
  SRV_Channels:set_output_pwm_chan_timeout(ch, pwm, 1000)
end

-- config
local config = {}
config["motor"] = {}
config["motor"][1] = {}
config["motor"][2] = {}
config["motor"][3] = {}
config["motor"][4] = {}
config["motor"][5] = {}
config["motor"][6] = {}
config["motor"][7] = {}
config["motor"][8] = {}
config["motor"][9] = {}
config["motor"][1]["func_str"] = "Motor1"
config["motor"][2]["func_str"] = "Motor2"
config["motor"][3]["func_str"] = "Motor3"
config["motor"][4]["func_str"] = "Motor4"
config["motor"][5]["func_str"] = "Motor5"
config["motor"][6]["func_str"] = "Motor6"
config["motor"][7]["func_str"] = "Motor7"
config["motor"][8]["func_str"] = "Motor8"
config["motor"][9]["func_str"] = "Motor9"
config["motor"][1]["func_idx"] = servo_function_options_str_to_index(config["motor"][1]["func_str"])
config["motor"][2]["func_idx"] = servo_function_options_str_to_index(config["motor"][2]["func_str"])
config["motor"][3]["func_idx"] = servo_function_options_str_to_index(config["motor"][3]["func_str"])
config["motor"][4]["func_idx"] = servo_function_options_str_to_index(config["motor"][4]["func_str"])
config["motor"][5]["func_idx"] = servo_function_options_str_to_index(config["motor"][5]["func_str"])
config["motor"][6]["func_idx"] = servo_function_options_str_to_index(config["motor"][6]["func_str"])
config["motor"][7]["func_idx"] = servo_function_options_str_to_index(config["motor"][7]["func_str"])
config["motor"][8]["func_idx"] = servo_function_options_str_to_index(config["motor"][8]["func_str"])
config["motor"][9]["func_idx"] = servo_function_options_str_to_index(config["motor"][9]["func_str"])

-- @brief test1
-- 以下を取得し随時表示
-- 1. 機体のフライトモード
-- 2. アームド状態
-- 3. モータ1-4のPWM
function test1()
  local mode  = get_mode()
  local armed = is_armed()
  local pwm1  = get_output_pwm(config["motor"][1]["func_idx"])
  local pwm2  = get_output_pwm(config["motor"][2]["func_idx"])
  local pwm3  = get_output_pwm(config["motor"][3]["func_idx"])
  local pwm4  = get_output_pwm(config["motor"][4]["func_idx"])
  send_text(6, "current_mode:"..copter_mode[mode])
  send_text(6, "is_armed:"..tostring(armed))
  send_text(6, "Motor1 PWM:"..pwm1)
  send_text(6, "Motor2 PWM:"..pwm2)
  send_text(6, "Motor3 PWM:"..pwm3)
  send_text(6, "Motor4 PWM:"..pwm4)
end

-- @brief test2
-- モータのPWMを設定
function test2()
  local pwm1  = get_output_pwm(config["motor"][1]["func_idx"])
  send_text(6, "Motor1 PWM:"..pwm1)
  set_servo_output_pwm(config["motor"][1]["func_str"], 1100)
  local pwm1  = get_output_pwm(config["motor"][1]["func_idx"])
  send_text(6, "Motor1 PWM:"..pwm1)
end

-- @ brief test3
-- RCチャンネルの入力を取得
function test3()
  local pwm1 = rc:get_pwm(1)
  local pwm2 = rc:get_pwm(2)
  local pwm3 = rc:get_pwm(3)
  local pwm4 = rc:get_pwm(4)
  local pwm5 = rc:get_pwm(5)
  local pwm6 = rc:get_pwm(6)
  local pwm7 = rc:get_pwm(7)
  local pwm8 = rc:get_pwm(8)
  local pwm9 = rc:get_pwm(9)
  local pwm10 = rc:get_pwm(10)
  local pwm11 = rc:get_pwm(11)
  local pwm12 = rc:get_pwm(12)
  local pwm13 = rc:get_pwm(13)
  local pwm14 = rc:get_pwm(14)
  local pwm15 = rc:get_pwm(15)
  local pwm16 = rc:get_pwm(16)
  Logger.Write(string.format("pwm1:%d, pwm2:%d, pwm3:%d, pwm4:%d, pwm5:%d, pwm6:%d, pwm7:%d, pwm8:%d\n", pwm1, pwm2, pwm3, pwm4, pwm5, pwm6, pwm7, pwm8))
  Logger.Write(string.format("pwm9:%d, pwm10:%d, pwm11:%d, pwm12:%d, pwm13:%d, pwm14:%d, pwm15:%d, pwm16:%d\n", pwm9, pwm10, pwm11, pwm12, pwm13, pwm14, pwm15, pwm16))
  -- send_text(6, "RC CH1's PWM:"..pwm1)
  -- send_text(6, "RC CH2's PWM:"..pwm2)
end

function get_rc_inputs()
  local scripting_rc_1 = rc:find_channel_for_option(300)
  if scripting_rc_1 then
    -- Logger.Write(string.format("Scripting in 1:%d\n", scripting_rc_1:norm_input()))
    gcs:send_text(0, "Scripting in 1:" .. tostring(scripting_rc_1:norm_input()))
  end
end

function get_flight_plan()
  local num_commands = mission:num_commands()
  gcs:send_text(6, "num_commands:"..num_commands)
  for i = 0, num_commands - 1 do 
    local wp = mission:get_item(i)
    local log, param1, param2, param3, param4, x, y, z, seq, command, frame, current
    if wp then
      param1  = string.format("wp[%d] param1:%f", i, wp:param1())
      param2  = string.format("wp[%d] param2:%f", i, wp:param2())
      param3  = string.format("wp[%d] param3:%f", i, wp:param3())
      param4  = string.format("wp[%d] param4:%f", i, wp:param4())
      x       = string.format("wp[%d] x:%d", i, wp:x())
      y       = string.format("wp[%d] y:%d", i, wp:y())
      z       = string.format("wp[%d] z:%f", i, wp:z())
      seq     = string.format("wp[%d] seq:%d", i, wp:seq())
      command = string.format("wp[%d] command:%d", i, wp:command())
      frame   = string.format("wp[%d] frame:%d", i, wp:frame())
      current = string.format("wp[%d] current:%d", i, wp:current())
      gcs:send_text(6, param1)
      gcs:send_text(6, param2)
      gcs:send_text(6, param3)
      gcs:send_text(6, param4)
      gcs:send_text(6, x)
      gcs:send_text(6, y)
      gcs:send_text(6, z)
      gcs:send_text(6, seq)
      gcs:send_text(6, command)
      gcs:send_text(6, frame)
      gcs:send_text(6, current)
      log = tostring(i) .. "," .. param1 .. "," .. param2 .. "," .. param3 .. "," .. param4 .. "," .. x .. "," .. y .. "," .. z .. "," .. seq .. "," .. command .. "," .. frame .. "," .. current .. "\n"
      Logger.Write(log)
    end
  end  
end

-- init
function init()
  Logger.Write("---\n")
  get_flight_plan()
  gcs:send_text(6, "init done.")
end

-- update
function update()
  local current_nav_index = mission:get_current_nav_index()
  local rc_ch1 = rc:get_pwm(1)
  local servo9_func = 94
  local servo9_chan = SRV_Channels:find_channel(servo9_func)
  local servo9_pwm = SRV_Channels:get_output_pwm(servo9_func)
  Logger.Write(string.format("nav_idx:%d, rc_ch1:%d, servo9_pwm%d\n", current_nav_index, rc_ch1, servo9_pwm));
  if current_nav_index == 1 or current_nav_index == 3 then
    SRV_Channels:set_output_pwm_chan_timeout(servo9_chan, rc_ch1, 1000)
  else
    SRV_Channels:set_output_pwm_chan_timeout(servo9_chan, 1000, 1000)
  end
  
  return update, 800
end

-- main
init()
return update(), 1000